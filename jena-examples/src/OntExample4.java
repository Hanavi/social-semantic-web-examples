import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;

import java.util.Iterator;

public class OntExample4 {

    public static void main(String[] args) {
        // create the base model
        String sourceURL = "https://www.w3.org/TR/2003/PR-owl-guide-20031215/wine";
        OntModel base = ModelFactory.createOntologyModel();
        base.read( sourceURL, "RDF/XML" );

        String namespace = "http://www.w3.org/TR/2003/PR-owl-guide-20031209/wine#";
        OntClass wine = base.getOntClass(namespace + "RedBordeaux");
        Individual atakanWine = base.createIndividual(namespace + "AtakanWine-madebyAtakan", wine);

        for (Iterator<Resource> i = atakanWine.listRDFTypes(true); i.hasNext();){
            System.out.println(atakanWine.getURI() + " is asserted in class " + i.next());
        }

        OntModel inf = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM_MICRO_RULE_INF, base);

        atakanWine = inf.getIndividual(namespace + "AtakanWine-madebyAtakan");
        for (Iterator<Resource> i = atakanWine.listRDFTypes(true); i.hasNext(); ){
            System.out.println(atakanWine.getURI() + " is inferred to be in class " + i.next());
        }
    }
}
