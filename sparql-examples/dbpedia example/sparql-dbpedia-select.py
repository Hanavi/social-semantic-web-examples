from SPARQLWrapper import SPARQLWrapper, JSON

sparql = SPARQLWrapper("http://dbpedia.org/sparql")
sparql.setQuery("""
    PREFIX dbr: <http://dbpedia.org/resource/>
    PREFIX dbo: <http://dbpedia.org/ontology/>
    SELECT ?x
    WHERE { ?x dbo:location dbr:Turkey }
""")
sparql.setReturnFormat(JSON)
results = sparql.query().convert()

with open("select-results.txt", "w", encoding="utf-8") as f:
    for result in results["results"]["bindings"]:
        f.write(result["x"]["value"] + "\n")


